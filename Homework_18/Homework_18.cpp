﻿#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;

template <class AnyType>
class MyStack {
private:

	AnyType* StackArray = NULL;
	int size = 0;

public:

	MyStack()
	{
	}

	~MyStack()
	{
	}

	void push(AnyType value)
	{
		AnyType* NewArray = new AnyType[size + 1];
		cout << "Add Element " << value << endl;

		for (int i = 0; i < size; i++)
		{
			NewArray[i] = StackArray[i];
		}
		NewArray[size] = value;
		delete[] StackArray;
		size++;
		StackArray = NewArray;
	}

	void pop()
	{
		--size;
	}

	void Print()
	{
		for (int i = 0; i < size; i++)
		{
			std::cout << *(StackArray + i) << " ";
		}
		std::cout << std::endl;
	}

	void Del()
	{
		delete[] StackArray;
	}

};

int main()
{
	MyStack<int> IntStack;

	IntStack.push(1);
	IntStack.push(2);
	IntStack.push(3);
	IntStack.push(4);
	IntStack.push(5);
	IntStack.Print();
	IntStack.pop();
	IntStack.Print();
	IntStack.pop();
	IntStack.Print();
	IntStack.pop();
	IntStack.Print();
	IntStack.pop();
	IntStack.Print();
	IntStack.Del();

	return 0;
}